$(function(){
  var ii = {};
  ii.window_domain = "http://dev-coursefinder";
  ii.window_height = 900;
  var sendHeight = function () {
    var frameHeight = document.body.clientHeight || document.body.offsetHeight || document.body.scrollHeight;
    if (frameHeight != ii.window_height) {
        ii.window_height = frameHeight;
        window.parent.postMessage(ii.window_height, ii.window_domain);
    }
  };
  setInterval(sendHeight, 250);
});
